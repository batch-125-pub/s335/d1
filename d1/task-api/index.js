const express = require('express');

const mongoose = require('mongoose'); //this code is to be used on our db connection and to create our schema and model for our existing MongoDB atlas collection

const app = express(); //creating a server through the use of app.

const PORT = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

//mongoose.connect - is a way to connect our mongodb atlas db connection string to our server. 
//copy the connection string from mongodb database (in mongodb, go to overview > click Connect your application >copy the Connection string, then paste it here - make sure to edit/replace the username, password and name of the database (myFirstDatabase section) to the ones you created)
//Due to updates made by MongoDB Atlas developers, the default connection string is being flagged as an error. o skip that error or warning that we are going to encounter in the future, we will add the useNewUrlParser and useUnifiedTopology properties inside our mongoose.connect
mongoose.connect("mongodb+srv://sarahdidulo:Godlovesyou@cluster0.lxfuk.mongodb.net/b125-tasks?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology: true	
	}
).then(()=>{ //if the mongoose succeeded on the connection, then we will print console.log message
	console.log(`Successfully Connected to Database`);
}).catch((error)=>{ //handes error when the mongoose failed connect to our mongodb atlas database
	console.log(error);
})

//Schema - gives a structure of what kind of record/document we are going to contain on our database

//Schema() method - determines the structure of the documents to be written in the database
// Schema acts as blueprint to our data
//We used the Schema() constructor of the Mongoose dependency to create a new Schema object for our tasks collection
//The "new" keyword, creates a new Schema

const taskSchema = new mongoose.Schema({
	//Define the fields with their corresponding data type
	//For task, it needs a field called 'name' and 'status'
	// The field name has a data type of 'String'
	//The field status has a data type of 'Boolean' with a default value of 'false'
	name : String,
	//Default values are he predefined values for a field if we do not put any value
	status : {type: Boolean, default: false}
});

const userSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	userName: String,
	password: String
});

//Models - to perform the CRUD operations for our defined collection with their corresponding schema

//The Task variable will contain the model for our tasks collection and shall perform the CRUD operations
//The first parameter of the mongoose.model indicates the collection in where to store the data. Take note: the collection name must be written in singular form and the first letter of the name must be in uppercase
//The second parameter is used to specify the Schema/Blueprint of the documents that will be stored on the tasks collection
const Task = mongoose.model('Task', taskSchema);

const User = mongoose.model('User', userSchema);

/*Business Logic - Todo list application
	- CRUD operation for our Tasks collection
*/


app.post('/add-task', (req, res)=> {
	//call the model for our tasks collection: Task
	//create an instance of the task model and save it to ou database
	//creating a new Task with task name: 'PM Break' through the use of the Task Model
	//default for Status is already false
	//in postman, to test the input, go to Body, then click raw, then always choose JSON in the dropdown (input should be in JSON form)
	let newTask = new Task(
		{
			name: req.body.name
		}
	);

	//Telling our server that the newTask will now be saved as a new document to our Task collection on our database
	//.save() - saves a new document to our database
	//on our callback, it will receive 2 values, the error and the saved document
	//error value shall contain the error whenever there is an error encountered while we are saving our document
	//savedTask shall contain the newly saved document from the database once the saving process is successful
	newTask.save((error, savedTask)=>{
		if(error){
			console.log(error);
		} else {
			res.send(`New Task saved! ${savedTask}`);
		}
	});
});

app.post('/register', (req, res)=>{
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		userName: req.body.userName,
		password: req.body.password
	});

	newUser.save((error, savedUser)=>{
		if(error){
			console.log(error);
		} else {
			res.send(`New User: ${savedUser} is created`);
		}
	});
});

//Retrieve

app.get('/retrieve-tasks', (req, res)=>{
	//find({}) will retrieve all the documents from the tasks collection
	// the error on the callback will handle the errors encountered while retrieve the records
	//the records on the callback will handle the raw data from the database
	Task.find({}, (error, records)=>{
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	});
});

//retrieve tasks that are done, means the status = true
app.get('/retrieve-tasks-done', (req, res)=>{
	Task.find({status: true}, (error, records)=>{
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}	
	})
});

//update operation
app.put('/complete-task/:taskId', (req, res)=>{ 
	//res.send({urlParams: req.params.taskId });
	//1. Find the specific record using its ID
	//2. And update it

	//findByIdAndUpdate() -> 
	//source: https://mongoosejs.com/docs/api/model.html#model_Model.findByIdAndUpdate
	let taskId = req.params.taskId;
	//url parameters - these are the values defined on the URL.
	//to get the url parameters - req.params.<paramsName>
	//:taskId -> is a way to indicate that we are going to receive a url parameter. These are what we call a 'wilcard'

	Task.findByIdAndUpdate(taskId, {status: true}, (error, updatedTask)=>{
		if(error){	
			console.log(error);
		} else {
			res.send(`Task completed sucessfully`);
		}
	});


});

//Delete Operation
app.delete('/delete-task/:taskId', (req, res)=>{
	//findbyIdAndDelete() - finds the specific record using its Id and delete

	let taskId = req.params.taskId;
	Task.findByIdAndDelete(taskId, (error, deletedTask)=>{
		if(error){
			console.log(error);
		} else {
			res.send(`Task deleted!`);
		}
	});
});



app.listen(PORT, () => {
	console.log(`Server running at port ${PORT}`);
});

